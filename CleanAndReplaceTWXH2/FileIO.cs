﻿using System;
using System.IO;
using System.IO.Compression;

namespace CleanAndReplaceTWXH2
    {
    /// <summary>
    /// The base class used to contain all of the necessary things required to interact with the local file system.
    /// </summary>
    class FileIO
        {
        private const string thingworxStorage = @"C:\ThingworxStorage\";
        private const string thingworxWarFileName = @"\Thingworx.war";
        private const string thingworxBackupStorage = @"C:\ThingworxBackupStorage\";
        private const string keystorePasswordFileName = @"C:\ThingworxPlatform\keystore-password";

        /// <summary>
        /// Clean out all of the current Thingworx Directories and Files so that a new build can be deployed cleanly.
        /// </summary>
        /// <param name="keepEntityStorage">Boolean value for whether or not to keep entity storage while replacing or cleaning the current build.</param>
        /// <param name="onlyClean">Boolean value for whether or not to only clean, and not replace the current build.</param>
        public static void CleanTwxFiles(bool keepEntityStorage, bool onlyClean)
        {
            if (!TomcatServer.IsTomcatRunning())
            {
                if (!keepEntityStorage)
                {
                    if (Directory.Exists(thingworxStorage))
                    {
                        RecursiveInnerDelete(thingworxStorage);
                    }
                }

                if (File.Exists(thingworxStorage + "keystore.jks"))
                {
                    try
                    {
                        File.Delete(thingworxStorage + "keystore.jks");
                    }
                    catch (IOException)
                    {

                    }
                }

                if (Directory.Exists(TomcatServer.tomcatServerWebAppsThingworx))
                {
                    try
                    {
                        Directory.Delete(TomcatServer.tomcatServerWebAppsThingworx, true);
                    }
                    catch (IOException)
                    {
                        
                    }
                }

                if (!onlyClean)
                {
                    if (File.Exists(TomcatServer.tomcatServerWebApps + thingworxWarFileName))
                    {
                        File.Delete(TomcatServer.tomcatServerWebApps + thingworxWarFileName);
                    }
                }

                if (Directory.Exists(thingworxBackupStorage))
                {
                    RecursiveInnerDelete(thingworxBackupStorage);
                }

                if (File.Exists(keystorePasswordFileName))
                {
                    File.Delete(keystorePasswordFileName);
                }
            }
            else
            {
                CleanTwxFiles(keepEntityStorage, onlyClean);
            }
        }

        /// <summary>
        /// Extract the .zip file and place the inner Thingworx.war file in the Tomcat bin folder, so that the Tomcat server can be restarted.
        /// </summary>
        public static void ExtractAndPlaceWarFile()
        {
            string zipFileNameAndPath = ArtifactoryRequest.appDataLocalDirectory + Path.DirectorySeparatorChar + ArtifactoryRequest.TWXH2WarFileName;
            string extractPath = TomcatServer.tomcatServerWebApps;
            if (File.Exists(zipFileNameAndPath))
            {
                ZipFile.ExtractToDirectory(zipFileNameAndPath, extractPath);
                File.Delete(zipFileNameAndPath);
            }
        }

        /// <summary>
        /// Recuresive Delete of a directory without deleting the base directory -> unlike Directory.Delete(path, true),
        /// which removes the base directory as well as its contents recursively.
        /// TODO:: Make this method more resilient to active explorer windows blocking the delete of a directory.
        /// </summary>
        /// <param name="baseDir">The directory to be cleaned out.</param>
        private static void RecursiveInnerDelete(String baseDir)
        {
            DirectoryInfo di = new DirectoryInfo(baseDir);
            foreach (FileInfo file in di.GetFiles())
            {
                try
                {
                    file.Delete();
                }
                catch (IOException)
                {
                    
                }
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                try
                {
                    dir.Delete(true);
                }
                catch (IOException)
                {

                }
            }
        }
    }
}
