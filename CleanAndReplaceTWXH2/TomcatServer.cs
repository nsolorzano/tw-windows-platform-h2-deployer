﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace CleanAndReplaceTWXH2
    {
    /// <summary>
    /// The base class used to contain all of the necessary things required to interact with Apache Tomcat 8.
    /// </summary>
    class TomcatServer
        {
        public static string tomcatServerHome = GetTomcatInstallDirectory();
        public static string tomcatServerWebApps = tomcatServerHome + @"\webapps";
        public static string tomcatServerWebAppsThingworx = tomcatServerWebApps + @"\Thingworx";
        private const string tomcatRegistry64Path = @"SOFTWARE\Apache Software Foundation\Tomcat\8.5\Tomcat8";

        /// <summary>
        /// Sends the Tomcat Server Stop broadcast message.
        /// Requires that a %CATALINA_HOME% enviroment variable be set prior to execution.
        /// TODO: Use absolute path to Tomcat installation so that no environment variable is needed.
        /// </summary>
        public static void Stop()
        {
            var command = "\"" + tomcatServerHome + Path.DirectorySeparatorChar + "bin" + Path.DirectorySeparatorChar + "catalina.bat\" stop";
            ExecuteCommand(command);
        }

        /// <summary>
        /// Sends the Tomcat Server Start broadcast message.
        /// Requires that a %CATALINA_HOME% enviroment variable be set prior to execution.
        /// TODO: Use absolute path to Tomcat installation so that no environment variable is needed.
        /// </summary>
        public static void Start()
            {
            var command = "\"" + tomcatServerHome + Path.DirectorySeparatorChar + "bin" + Path.DirectorySeparatorChar + "catalina.bat\" start";
            ExecuteCommand(command);
            }

        /// <summary>
        /// Use to start and stop the Tomcat Server gracefully before and after replaceing the TWX War file.
        /// </summary>
        /// <param name="command">String representation of the command to be executed. White Space in this string will cause issues at runtime.</param>
        static void ExecuteCommand(string command)
            {
            var processInfo = new ProcessStartInfo("cmd.exe", "/c " + command)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardOutput = true
            };
            var process = Process.Start(processInfo);
            process.WaitForExit();
            process.Close();
            }

        /// <summary>
        /// Using the Windows Registry; finds the current installation directory of Apache Tomcat
        /// </summary>
        public static string GetTomcatInstallDirectory()
        {
            var installDir = string.Empty;
            foreach (var valueName in GetAllRegValueNames(tomcatRegistry64Path))
            {
                if (valueName.Equals("InstallPath"))
                {
                    installDir = GetRegValue(tomcatRegistry64Path, valueName).ToString();
                }
            }
            return installDir;
        }

        /// <summary>
        /// Discover from the currently running processes whether or not the Tomcat Server is running.
        /// </summary>
        /// <returns>True is Tomcat is running.</returns>
        public static Boolean IsTomcatRunning()
        {
            Process[] processlist = Process.GetProcesses();
            foreach (var process in processlist)
            {
                if (process.MainWindowTitle == "Tomcat")
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="view"></param>
        /// <param name="regPath"></param>
        /// <param name="hive"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetRegValueNames(RegistryView view, string regPath, RegistryHive hive = RegistryHive.LocalMachine)
        {
            return RegistryKey.OpenBaseKey(hive, view)?.OpenSubKey(regPath)?.G‌​etValueNames();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RegPath"></param>
        /// <param name="hive"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetAllRegValueNames(string RegPath, RegistryHive hive = RegistryHive.LocalMachine)
        {
            var reg64 = GetRegValueNames(RegistryView.Registry64, RegPath, hive);
            var reg32 = GetRegValueNames(RegistryView.Re‌​gistry32, RegPath, hive);
            var result = (reg64 != null && reg32 != null) ? reg64.Union(reg32) : (reg64 ?? reg32);
            return (result ?? new List<string>().AsEnumerable());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="view"></param>
        /// <param name="regPath"></param>
        /// <param name="ValueName"></param>
        /// <param name="hive"></param>
        /// <returns></returns>
        public static object GetRegValue(RegistryView view, string regPath, string ValueName, RegistryHive hive = RegistryHive.LocalMachine)
        {
            return RegistryKey.OpenBaseKey(hive, view)?.OpenSubKey(regPath)?.G‌​etValue(ValueName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RegPath"></param>
        /// <param name="ValueName"></param>
        /// <param name="hive"></param>
        /// <returns></returns>
        public static object GetRegValue(string RegPath, string ValueName, RegistryHive hive = RegistryHive.LocalMachine)
        {
            return GetRegValue(RegistryView.Registry64, RegPath, ValueName, hive) ?? GetRegValue(RegistryView.Re‌​gistry32, RegPath, ValueName, hive);
        }
    }
}
