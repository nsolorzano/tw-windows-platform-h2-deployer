﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Windows.Forms;

namespace CleanAndReplaceTWXH2
    {
    public partial class ThingworxWarFileReplacer : Form
        {
        private string selectedbuild;
        private string credentials;

        /// <summary>
        /// Public form class for this application
        /// </summary>
        public ThingworxWarFileReplacer()
            {
            InitializeComponent();

            /// Initialize Event Listeners
            executeButton.Click += ExecuteButton_Click;
            closeButton.Click += CloseButton_Click;
            keepEntityStorageLabel.Click += KeepEntityStorageLabel_Click;
            restartTomcatLabel.Click += RestartTomcatLabel_Click;
            onlyCleanLabel.Click += OnlyCleanLabel_Click;
            onlyCleanCheckBox.CheckStateChanged += OnlyCleanCheckBox_CheckStateChanged;
            loadAvailableBuildsButton.Click += LoadAvailableBuildsButton_Click;
            buildsComboBox.SelectionChangeCommitted += BuildsComboBox_SelectionChangeCommitted;
            buildsComboBox.TextChanged += BuildsComboBox_SelectionChangeCommitted;

            /// Set the current user's name in the <see cref="userNameTextBox"/> field.
            userNameTextBox.Text = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split('\\')[1];
        }

        /// <summary>
        /// Event handler used to reset the UI in the event that a new build was selected.
        /// </summary>
        /// <param name="sender">Control that raised the event.</param>
        /// <param name="e">Parameters passed from event handler.</param>
        private void BuildsComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (!executeButton.Enabled)
            {
                executeButton.Enabled = true;
                progressBar.Value = 0;
                consoleTextBox.Text = "Ready to Execute";
            }
            selectedbuild = buildsComboBox.Text;
        }

        /// <summary>
        /// Event handler used to invoke <see cref="ArtifactoryRequest.GetBuildsFromArtifactory"/> and conditionally set variables.
        /// </summary>
        /// <param name="sender">Control that raised the event.</param>
        /// <param name="e">Parameters passed from event handler.</param>
        private void LoadAvailableBuildsButton_Click(object sender, EventArgs e)
        {
            if (passwordTextBox.Text != "")
            {
                ArtifactoryRequest.GetBuildsFromArtifactory(ArtifactoryRequest.TWXPlatformH2_URL, GetCredentials());
                buildsComboBox.DataSource = null;
                buildsComboBox.DataSource = ArtifactoryRequest.builds;
                selectedbuild = buildsComboBox.Text;
            }
            else
            {
                MessageBox.Show("Please enter your credentials for the artifactory request.");
            }
        }

        /// <summary>
        /// Event handler user to disable controls in the event they are not required for the selected process.
        /// </summary>
        /// <param name="sender">Control that raised the event.</param>
        /// <param name="e">Parameters passed from event handler.</param>
        private void OnlyCleanCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            buildsComboBox.Enabled = onlyCleanCheckBox.Checked ? false : true;
            passwordTextBox.Enabled = onlyCleanCheckBox.Checked ? false : true;
        }

        /// <summary>
        /// Event handler user to invoke the main operational thread <see cref="ExecuteThread"/>.
        /// </summary>
        /// <param name="sender">Control that raised the event.</param>
        /// <param name="e">Parameters passed from event handler.</param>
        private void ExecuteButton_Click(object sender, EventArgs e)
        {
            if (onlyCleanCheckBox.Checked || (passwordTextBox.Text.Length > 0))
            {
                credentials = GetCredentials();
                Thread t = new Thread(() => { ExecuteThread(onlyCleanCheckBox.Checked, restartTomcatCheckBox.Checked, keepEntityStorageCheckBox.Checked); });
                t.Start();
            }
            else
            {
                MessageBox.Show("Please enter your credentials for the artifactory request.");
            }
        }

        /// <summary>
        /// Event handler used to close the application in the event that the Close button is clicked.
        /// </summary>
        /// <param name="sender">Control that raised the event.</param>
        /// <param name="e">Parameters passed from event handler.</param>
        private void CloseButton_Click(object sender, EventArgs e) => Close();

        /// <summary>
        /// Event Handler used to set the checkbox state in the event that the label itself is clicked - for better UX...
        /// </summary>
        /// <param name="sender">Control that raised the event.</param>
        /// <param name="e">Parameters passed from event handler.</param>
        private void KeepEntityStorageLabel_Click(object sender, EventArgs e)
        {
            keepEntityStorageCheckBox.Checked = keepEntityStorageCheckBox.Checked ? false : true;
        }

        /// <summary>
        /// Event Handler used to set the checkbox state in the event that the label itself is clicked - for better UX...
        /// </summary>
        /// <param name="sender">Control that raised the event.</param>
        /// <param name="e">Parameters passed from event handler.</param>
        private void RestartTomcatLabel_Click(object sender, EventArgs e)
        {
            restartTomcatCheckBox.Checked = restartTomcatCheckBox.Checked ? false : true;
        }

        /// <summary>
        /// Event Handler used to set the checkbox state in the event that the label itself is clicked - for better UX...
        /// </summary>
        /// <param name="sender">Control that raised the event.</param>
        /// <param name="e">Parameters passed from event handler.</param>
        private void OnlyCleanLabel_Click(object sender, EventArgs e)
        {
            onlyCleanCheckBox.Checked = onlyCleanCheckBox.Checked ? false : true;
        }

        /// <summary>
        /// Using the credentials of the current user creates an HTTP request Basic Auth set of credentials.
        /// </summary>
        /// <returns>Authentication header string.</returns>
        private string GetCredentials()
        {
            var encodedString = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(string.Format("{0}:{1}", userNameTextBox.Text, passwordTextBox.Text)));
            var client = new HttpClient();
            return (client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", encodedString)).ToString();
        }

        /// <summary>
        /// Main execution method used when all criteria has been met for the application to run it's actions.
        /// All calls to any Windows.Forms.Controls objects need to be thread safe!
        /// </summary>
        /// <param name="onlyClean">Boolean value for whether or not to only clean, and not replace the current build.</param>
        /// <param name="restartTomcat">Boolean value for whether or not to restart the Tomcat Server after replacing the build.</param>
        /// <param name="keepEntityStorage">Boolean value for whether or not to keep entity storage while replacing or cleaning the current build.</param>
        public void ExecuteThread(bool onlyClean, bool restartTomcat, bool keepEntityStorage)
        {
            SetProgressBarValue(0);
            SetControlState("executeButton", false);
            SetControlState("closeButton", false);
            SetControlState("passwordTextBox", false);
            SetControlState("buildsComboBox", false);

            ConsoleAppendText("Sending Stop message to the Tomcat Server.");
            TomcatServer.Stop();
            ProgressBarPerformStep();
            ConsoleAppendText("Cleaning out Thingworx Storage Files.");
            FileIO.CleanTwxFiles(keepEntityStorage, onlyClean);

            if (keepEntityStorage)
            {
                ConsoleAppendText("\t- Keeping Entity Storage!");
            }

            ProgressBarPerformStep();

            if (!onlyClean)
            {
                ConsoleAppendText("Getting the selected WAR file from Artifactory.");
                ProgressBarPerformStep();
                ArtifactoryRequest.GetH2WarFile(credentials, selectedbuild);
                ProgressBarPerformStep();
                ConsoleAppendText("Extracting the WAR file and placing it in the Tomcat WebApps Folder.");
                FileIO.ExtractAndPlaceWarFile();
            }

            if (restartTomcat)
            {
                ConsoleAppendText("Starting the Tomcat Server.");
                TomcatServer.Start();
            }

            SetProgressBarValue(100);
            ConsoleAppendText("Processing Complete.");

            SetControlState("closeButton", true);
            SetControlState("passwordTextBox", true);
            SetControlState("buildsComboBox", true);

            // Reset the controls for next execution run.
            SetControlState("onlyCleanCheckBox", false);
            SetControlState("keepEntityStorageCheckBox", false);
            SetControlState("restartTomcatCheckBox", false);
        }

        /// <summary>
        /// Delegate method for resetting the state of the progress bar.
        /// </summary>
        delegate void SetProgressBarValueDelegate(int value);

        /// <summary>
        /// Used to invoke a delegate method for resetting the progress bar.
        /// </summary>
        private void SetProgressBarValue(int value)
        {
            if (progressBar.InvokeRequired)
            {
                SetProgressBarValueDelegate d = new SetProgressBarValueDelegate(SetProgressBarValue);
                Invoke(d, new object[] { value });
            }
            else
            {
                progressBar.Value = value;
            }
        }

        /// <summary>
        /// Delegate method for setting various states on form controls.
        /// </summary>
        /// <param name="constrolName"></param>
        /// <param name="controlState"></param>
        delegate void SetControlStateDelegate(string constrolName, bool controlState);

        /// <summary>
        /// Used to invoke a delegate method for setting various states on form controls.
        /// </summary>
        /// <param name="controlName">The name of the control.</param>
        /// <param name="controlState">The state to be set on the control.</param>
        private void SetControlState(string controlName, bool controlState)
        {
            Control control = Controls.Find(controlName, true)[0];
            if (control.InvokeRequired)
            {
                SetControlStateDelegate d = new SetControlStateDelegate(SetControlState);
                Invoke(d, new object[] { controlName, controlState });
            }
            else
            {
                
                if (control is CheckBox)
                {
                    ((CheckBox)control).Checked = controlState;
                }
                else
                {
                    control.Enabled = controlState;
                }
            }
        }

        /// <summary>
        /// Delegate method for performing a step of the progress bar in the application.
        /// </summary>
        delegate void PerformStepDelegate();

        /// <summary>
        /// Used to invoke a delegate method for performing a step of the progress bar in the application.
        /// </summary>
        private void ProgressBarPerformStep()
        {
            if (progressBar.InvokeRequired)
            {
                PerformStepDelegate d = new PerformStepDelegate(ProgressBarPerformStep);
                Invoke(d);
            }
            else
            {
                progressBar.PerformStep();
            }
        }

        /// <summary>
        /// Delegate method for setting text in the console view of the application.
        /// </summary>
        /// <param name="text">The text to be set.</param>
        delegate void AppendConsoleTextDelegate(string text);

        /// <summary>
        /// Used to invoke a delegate method for setting text in the console view of the application.
        /// </summary>
        /// <param name="text">The text to be set.</param>
        private void ConsoleAppendText(string text)
        {
            if (consoleTextBox.InvokeRequired)
            {
                AppendConsoleTextDelegate d = new AppendConsoleTextDelegate(ConsoleAppendText);
                Invoke(d, new object[] { text });
            }
            else
            {
                consoleTextBox.AppendText(text);
                consoleTextBox.AppendText(Environment.NewLine);
            }
        }
    }
}
