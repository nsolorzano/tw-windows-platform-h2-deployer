﻿using System;

namespace CleanAndReplaceTWXH2
    {
    partial class ThingworxWarFileReplacer
        {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        [MTAThread]
        private void InitializeComponent()
            {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThingworxWarFileReplacer));
            this.executeButton = new System.Windows.Forms.Button();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.userNameLabel = new System.Windows.Forms.Label();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.userNameTextBox = new System.Windows.Forms.TextBox();
            this.loadAvailableBuildsButton = new System.Windows.Forms.Button();
            this.buildsComboBox = new System.Windows.Forms.ComboBox();
            this.onlyCleanLabel = new System.Windows.Forms.Label();
            this.onlyCleanCheckBox = new System.Windows.Forms.CheckBox();
            this.consoleTextBox = new System.Windows.Forms.TextBox();
            this.restartTomcatCheckBox = new System.Windows.Forms.CheckBox();
            this.restartTomcatLabel = new System.Windows.Forms.Label();
            this.keepEntityStorageCheckBox = new System.Windows.Forms.CheckBox();
            this.keepEntityStorageLabel = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.closeButton = new System.Windows.Forms.Button();
            this.groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // executeButton
            // 
            this.executeButton.Location = new System.Drawing.Point(558, 258);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(94, 23);
            this.executeButton.TabIndex = 0;
            this.executeButton.Text = "Execute";
            this.executeButton.UseVisualStyleBackColor = true;
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.passwordLabel);
            this.groupBox.Controls.Add(this.userNameLabel);
            this.groupBox.Controls.Add(this.passwordTextBox);
            this.groupBox.Controls.Add(this.userNameTextBox);
            this.groupBox.Controls.Add(this.loadAvailableBuildsButton);
            this.groupBox.Controls.Add(this.buildsComboBox);
            this.groupBox.Controls.Add(this.onlyCleanLabel);
            this.groupBox.Controls.Add(this.onlyCleanCheckBox);
            this.groupBox.Controls.Add(this.consoleTextBox);
            this.groupBox.Controls.Add(this.restartTomcatCheckBox);
            this.groupBox.Controls.Add(this.restartTomcatLabel);
            this.groupBox.Controls.Add(this.keepEntityStorageCheckBox);
            this.groupBox.Controls.Add(this.keepEntityStorageLabel);
            this.groupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox.Location = new System.Drawing.Point(13, 8);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(748, 244);
            this.groupBox.TabIndex = 1;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "Settings";
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Location = new System.Drawing.Point(369, 24);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(71, 16);
            this.passwordLabel.TabIndex = 14;
            this.passwordLabel.Text = "Password:";
            // 
            // userNameLabel
            // 
            this.userNameLabel.AutoSize = true;
            this.userNameLabel.Location = new System.Drawing.Point(16, 24);
            this.userNameLabel.Name = "userNameLabel";
            this.userNameLabel.Size = new System.Drawing.Size(80, 16);
            this.userNameLabel.TabIndex = 13;
            this.userNameLabel.Text = "User Name:";
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(443, 21);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '*';
            this.passwordTextBox.Size = new System.Drawing.Size(296, 22);
            this.passwordTextBox.TabIndex = 12;
            // 
            // userNameTextBox
            // 
            this.userNameTextBox.Location = new System.Drawing.Point(102, 21);
            this.userNameTextBox.Name = "userNameTextBox";
            this.userNameTextBox.Size = new System.Drawing.Size(248, 22);
            this.userNameTextBox.TabIndex = 11;
            this.userNameTextBox.Tag = "";
            // 
            // loadAvailableBuildsButton
            // 
            this.loadAvailableBuildsButton.Image = ((System.Drawing.Image)(resources.GetObject("loadAvailableBuildsButton.Image")));
            this.loadAvailableBuildsButton.Location = new System.Drawing.Point(719, 50);
            this.loadAvailableBuildsButton.Name = "loadAvailableBuildsButton";
            this.loadAvailableBuildsButton.Size = new System.Drawing.Size(22, 23);
            this.loadAvailableBuildsButton.TabIndex = 10;
            this.loadAvailableBuildsButton.UseVisualStyleBackColor = true;
            // 
            // buildsComboBox
            // 
            this.buildsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.buildsComboBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buildsComboBox.FormattingEnabled = true;
            this.buildsComboBox.Location = new System.Drawing.Point(370, 50);
            this.buildsComboBox.Name = "buildsComboBox";
            this.buildsComboBox.Size = new System.Drawing.Size(343, 24);
            this.buildsComboBox.TabIndex = 9;
            // 
            // onlyCleanLabel
            // 
            this.onlyCleanLabel.AutoSize = true;
            this.onlyCleanLabel.Location = new System.Drawing.Point(392, 81);
            this.onlyCleanLabel.Name = "onlyCleanLabel";
            this.onlyCleanLabel.Size = new System.Drawing.Size(85, 16);
            this.onlyCleanLabel.TabIndex = 8;
            this.onlyCleanLabel.Text = "ONLY Clean!";
            // 
            // onlyCleanCheckBox
            // 
            this.onlyCleanCheckBox.AutoSize = true;
            this.onlyCleanCheckBox.Location = new System.Drawing.Point(371, 81);
            this.onlyCleanCheckBox.Name = "onlyCleanCheckBox";
            this.onlyCleanCheckBox.Size = new System.Drawing.Size(15, 14);
            this.onlyCleanCheckBox.TabIndex = 7;
            this.onlyCleanCheckBox.UseVisualStyleBackColor = true;
            // 
            // consoleTextBox
            // 
            this.consoleTextBox.BackColor = System.Drawing.SystemColors.WindowText;
            this.consoleTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.consoleTextBox.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consoleTextBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.consoleTextBox.Location = new System.Drawing.Point(6, 104);
            this.consoleTextBox.Multiline = true;
            this.consoleTextBox.Name = "consoleTextBox";
            this.consoleTextBox.ReadOnly = true;
            this.consoleTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.consoleTextBox.Size = new System.Drawing.Size(736, 134);
            this.consoleTextBox.TabIndex = 6;
            this.consoleTextBox.WordWrap = false;
            // 
            // restartTomcatCheckBox
            // 
            this.restartTomcatCheckBox.AutoSize = true;
            this.restartTomcatCheckBox.Location = new System.Drawing.Point(13, 73);
            this.restartTomcatCheckBox.Name = "restartTomcatCheckBox";
            this.restartTomcatCheckBox.Padding = new System.Windows.Forms.Padding(5);
            this.restartTomcatCheckBox.Size = new System.Drawing.Size(25, 24);
            this.restartTomcatCheckBox.TabIndex = 5;
            this.restartTomcatCheckBox.UseVisualStyleBackColor = true;
            // 
            // restartTomcatLabel
            // 
            this.restartTomcatLabel.AutoSize = true;
            this.restartTomcatLabel.Location = new System.Drawing.Point(44, 73);
            this.restartTomcatLabel.Name = "restartTomcatLabel";
            this.restartTomcatLabel.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.restartTomcatLabel.Size = new System.Drawing.Size(256, 26);
            this.restartTomcatLabel.TabIndex = 4;
            this.restartTomcatLabel.Text = "Restart Tomcat Server After Processing?";
            // 
            // keepEntityStorageCheckBox
            // 
            this.keepEntityStorageCheckBox.AutoSize = true;
            this.keepEntityStorageCheckBox.Location = new System.Drawing.Point(13, 49);
            this.keepEntityStorageCheckBox.Name = "keepEntityStorageCheckBox";
            this.keepEntityStorageCheckBox.Padding = new System.Windows.Forms.Padding(5);
            this.keepEntityStorageCheckBox.Size = new System.Drawing.Size(25, 24);
            this.keepEntityStorageCheckBox.TabIndex = 3;
            this.keepEntityStorageCheckBox.UseVisualStyleBackColor = true;
            // 
            // keepEntityStorageLabel
            // 
            this.keepEntityStorageLabel.AutoSize = true;
            this.keepEntityStorageLabel.Location = new System.Drawing.Point(44, 49);
            this.keepEntityStorageLabel.Name = "keepEntityStorageLabel";
            this.keepEntityStorageLabel.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.keepEntityStorageLabel.Size = new System.Drawing.Size(138, 26);
            this.keepEntityStorageLabel.TabIndex = 2;
            this.keepEntityStorageLabel.Text = "Keep Entity Storage?";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 257);
            this.progressBar.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(532, 23);
            this.progressBar.Step = 20;
            this.progressBar.TabIndex = 2;
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(667, 258);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(94, 23);
            this.closeButton.TabIndex = 3;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // ThingworxWarFileReplacer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(773, 292);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.executeButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ThingworxWarFileReplacer";
            this.Text = "Thingworx WAR File ReplacerThing";
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            this.ResumeLayout(false);

            }
        
        #endregion
        private System.Windows.Forms.Button executeButton;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.CheckBox keepEntityStorageCheckBox;
        private System.Windows.Forms.Label keepEntityStorageLabel;
        private System.Windows.Forms.CheckBox restartTomcatCheckBox;
        private System.Windows.Forms.Label restartTomcatLabel;
        private System.Windows.Forms.TextBox consoleTextBox;
        private System.Windows.Forms.Label onlyCleanLabel;
        private System.Windows.Forms.CheckBox onlyCleanCheckBox;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.ComboBox buildsComboBox;
        private System.Windows.Forms.Button loadAvailableBuildsButton;
        private System.Windows.Forms.TextBox userNameTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label userNameLabel;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.ProgressBar progressBar;
    }
}

