﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace CleanAndReplaceTWXH2
{
    /// <summary>
    /// The base class used to contain all of the necessary things required to interact with Artifactory.
    /// </summary>
    class ArtifactoryRequest
        {
        // Private strings that could/can be set in a configuration file of sorts... TODO.
        private const string artifactoryRoot = "https://artifactory.rd2.thingworx.io/artifactory/";
        private const string artifactoryRepository = "twxlibs-snapshot-local/";
        private const string TWXH2RepoPath = "com/thingworx/Thingworx-Platform-H2/";
        private const string TWXPlatformH2 = "Thingworx-Platform-H2";

        // Public strings that shouldn't change.
        public const string TWXPlatformH2_URL = artifactoryRoot + artifactoryRepository + TWXH2RepoPath;
        public static string appDataLocalDirectory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

        // Public fields and collections
        public static List<string> builds = new List<string>();
        public static string TWXH2WarFileName;

        /// <summary>
        /// Process the Web Request used to retrieve the selected WAR file from Artifactory
        /// </summary>
        /// <param name="credentials">The current users' credentials to be used for the web request.</param>
        /// <param name="selectedBuild">The selected build to be retreived in the web request.</param>
        public static void GetH2WarFile(string credentials, string selectedBuild)
        {
            TWXH2WarFileName = TWXPlatformH2 + "-" + selectedBuild.Trim('/') + ".zip";
            var requestString = artifactoryRoot + artifactoryRepository + TWXH2RepoPath + selectedBuild + TWXH2WarFileName;
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Headers.Add(HttpRequestHeader.Authorization, credentials);
                    wc.DownloadFile(new Uri(requestString), appDataLocalDirectory + Path.DirectorySeparatorChar + TWXH2WarFileName);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Process the Web Request for populating the ComboBox of Builds, required to specifiy a desired build.
        /// </summary>
        /// <param name="TWXH2RepoPathURL">The full UNC path to the Thingworx H2 Artifactory Repository.</param>
        /// <param name="credentials">The current users' credentials to be used for the web request.</param>
        /// <returns></returns>
        public static void GetBuildsFromArtifactory(string TWXH2RepoPathURL, string credentials)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

            using (WebClient wc = new WebClient())
            {
                wc.Headers.Add(HttpRequestHeader.Authorization, credentials);
                wc.Headers.Add(HttpRequestHeader.Accept, "text/html");
                doc.LoadHtml(wc.DownloadString(TWXH2RepoPathURL));
            }

            if (doc != null)
            {
                foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
                {
                    var build = link.GetAttributeValue("href", "");

                    // First link on the page is to navigate backwards and the last is a file we don't care about.
                    if (!build.Contains("../") && !build.Contains("maven"))
                    {
                        builds.Add(build);
                    }
                }
            }
            else
            {
                MessageBox.Show("Missing document content...");
            }
            // Because we want the builds in a last-first order for usability.
            builds.Reverse();
        }
    }
}
